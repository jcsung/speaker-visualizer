import * as d3 from 'd3';
import React, {Component} from 'react';

import SoundObject from './sound_object';
import SoundWave from './sound_wave';

import {SOUND_SOURCE, SOUND_TARGET} from './sound_object';

export const NO_ACTION = 'NO ACTION TAKEN';
export const CREATE_TARGET = 'CREATING SOUND TARGET';

class Visualization extends Component {
	constructor(props) {
		super(props);

		this.state = {
			clickLoc: {},
			objects: [
				{
					highlighted: false,
					id: 0,
					r: 10,
					selected: false,
					type: SOUND_SOURCE,
					x: 250,
					y: 400
				},
				{
					highlighted: false,
					id: 1,
					r: 10,
					selected: false,
					type: SOUND_TARGET,
					x: 100,
					y: 100
				},
				{
					highlighted: false,
					id: 2,
					r: 10,
					selected: false,
					type: SOUND_TARGET,
					x: 400,
					y: 100
				},
				{
					highlighted: false,
					id: 3,
					r: 10,
					selected: false,
					type: SOUND_SOURCE,
					x: 123,
					y: 456
				},
				{
					highlighted: false,
					id: 4,
					r: 10,
					selected: false,
					type: SOUND_TARGET,
					x: 324,
					y: 10
				}
			]
		};
	}

	_deleteObject = event => {
		const {
			objects
		} = this.state;

		const deletionId = parseInt(event.target.attributes[0].value, 10);

		this.setState(
			{
				objects: objects.filter(
					val => val.id !== deletionId
				)
			}
		);
	}

	_getMouseCoordinates = event => {
		const {
			chart
		} = this.refs;

		const [x, y] = d3.clientPoint(chart, event);

		return {
			x,
			y
		};
	}

	_generateNewId = () => {
		const {
			objects
		} = this.state;

		const maxId = objects.length ? (
			objects.reduce(
				(prev, cur) => {
					return cur.id > prev ? cur.id : prev;
				},
				-1
			)
		) : (
			0
		);

		return maxId + 1;
	}

	_onMouseDown = event => {
		const coords = this._getMouseCoordinates(event);

		// console.log('Mouse Down', coords);

		// console.log(event.target.buttons);

		this.setState(
			{
				clickLoc: coords
			}
		);
	}

	_onMouseUp = event => {
		const {
			clickLoc
		} = this.state;

		// console.log(event);

		const coords = this._getMouseCoordinates(event);

		// console.log('Mouse Up', coords);

		const mouseDownNeighbors = this._nearestNeighbors(clickLoc.x, clickLoc.y);
		const mouseUpNeighbors = this._nearestNeighbors(coords.x, coords.y);

		// console.log('Neighborhood onMouseDown:', mouseDownNeighbors);
		// console.log('Neighborhood onMouseup:', mouseUpNeighbors);

		// If mouse down was on an empty spot, create new target there
		if (!mouseDownNeighbors.length) {
			// console.log('No nearest neighbors on mouse down.  Create sound target.');
			this._createSoundTarget(clickLoc.x, clickLoc.y);
		}
		else {
			const nearestId = mouseDownNeighbors[0];

			// If start and empty location are the same, simply
			// select or deselect the 0th index

			this._toggleObjectSelection(nearestId);

			if (!(clickLoc.x === coords.x 	& clickLoc.y === coords.y)) {
				// console.log('Coordinates not identical on mouse down and up.  Move location of id', nearestId, 'to', coords.x, coords.y);
				this._moveObject(nearestId, coords.x, coords.y);
			}
			else {
				// console.log('identical coordinates before and after');
			}
		}
	}

	_moveObject = (id, x, y) => {
		const {
			objects
		} = this.state;

		this.setState(
			{
				objects: objects.map(
					obj => {
						if (obj.id === id) {
							obj.x = x;
							obj.y = y;
						}

						return obj;
					}
				)
			}
		);
	}

	_checkSelectionStatus = id => {
		const {
			objects
		} = this.state;

		const selected = objects.filter(obj => obj.id === id);

		return selected.length ? selected[0].selected : false;
	}

	_setObjectSelection = (id, selected) => {
		const {
			objects
		} = this.state;

		this.setState(
			{
				objects: objects.map(
					obj => {
						if (obj.id === id) {
							obj.selected = selected;
						}
						else {
							obj.selected = false;
						}

						return obj;
					}
				)
			}
		);
	}

	_deselectObject = id => {
		this._setObjectSelection(id, false);
	}

	_selectObject = id => {
		this._setObjectSelection(id, true);
	}

	_toggleObjectSelection = id => {
		const {
			objects
		} = this.state;

		const objs = objects.filter(obj => obj.id === id);

		if (objs.length) {
			this._setObjectSelection(id, !objs[0].selected);
		}
	}

	_createSoundTarget = (x, y) => {
		// console.log('create target at:', x, y);
		const {
			objects
		} = this.state;

		const latestId = this._generateNewId();

		this.setState(
			{
				objects: [
					...objects.map(
						obj => {
							obj.selected = false;

							return obj;
						}
					),
					{
						highlighted: false,
						id: latestId,
						r: 10,
						selected: true,
						type: SOUND_TARGET,
						x,
						y
					}
				]
			}
		);
	}

	_distance2 = (x1, y1, x2, y2) => {
		return Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2);
	}

	_getAngle = (left, src, right) => {
		const a2 = this._distance2(left.x, left.y, src.x, src.y);
		const b2 = this._distance2(right.x, right.y, src.x, src.y);
		const c2 = this._distance2(left.x, left.y, right.x, right.y);

		const numer = c2 - a2 - b2;
		const denom = -2 * Math.sqrt(a2 * b2);

		return denom ? (
			`${(Math.acos(numer / denom) * 180 / Math.PI).toFixed(2)}°`
		) : (
			'N/A'
		);
	}

	_nearestNeighbors = (x, y) => {
		const {
			objects
		} = this.state;

		return objects.reduce(
			(prev, cur) => {
				const distance2 = this._distance2(x, y, cur.x, cur.y);
				const radius2 = cur.r * cur.r;

				return distance2 <= radius2 ? [...prev, cur] : prev;
			},
			[]
		).map(
			obj => obj.id
		);
	}

	render = () => {
		const {
			height,
			width
		} = this.props;

		const {
			objects
		} = this.state;

		const sources = objects.filter(obj => obj.type === SOUND_SOURCE);
		const targets = objects.filter(obj => obj.type === SOUND_TARGET);

		let i = 0;
		let j = 0;

		return (
			<div className="visualization-container">
				<svg
					className="speaker-visualizer-container"
					height={height}
					onMouseDown={event => this._onMouseDown(event)}
					onMouseUp={event => this._onMouseUp(event)}
					ref="chart"
					width={width}
				>
					{
						objects.map(
							obj => (
								<SoundObject
									highlighted={obj.highlighted}
									id={obj.id}
									key={i++}
									r={obj.r}
									selected={obj.selected}
									type={obj.type}
									x={obj.x}
									y={obj.y}
								/>
							)
						)
					}
					{
						sources.reduce(
							(prev, cur, sIndex) => {
								const lines = targets.map(
									obj => (
										<SoundWave
											highlighted={false}
											key={i++}
											x1={cur.x}
											y1={cur.y}
											x2={obj.x}
											y2={obj.y}
										/>
									)
								);

								return [...prev, ...lines];
							},
							[]
						)
					}
				</svg>

				<div className="speaker-visualizer-data-container">
					<div className="speaker-visualizer-data-info-container">
						<div className="speaker-visualizer-data-info-title">
							Sources
						</div>

						<div className="speaker-visualizer-data-info-body">
							{
								sources.map(
									(obj, index) => (
										<div
											className="object-list"
											key={index}
										>
											<span className="content">
												Object {obj.id}: ({obj.x}, {obj.y})
											</span>

											<span className="controls">
												<button
													onClick={event => this._deleteObject(event)}
													objectid={obj.id}
												>
													Delete
												</button>
											</span>
										</div>
									)
								)
							}
						</div>
					</div>

					<div className="speaker-visualizer-data-info-container">
						<div className="speaker-visualizer-data-info-title">
							Targets
						</div>

						<div className="speaker-visualizer-data-info-body">
							{
								targets.map(
									(obj, index) => (
										<div
											className="object-list"
											key={index}
										>
											<span className="content">
												Object {obj.id}: ({obj.x}, {obj.y})
											</span>
											<span className="controls">
												<button
													onClick={event => this._deleteObject(event)}
													objectid={obj.id}
												>
													Delete
												</button>
											</span>
										</div>
									)
								)
							}
						</div>
					</div>

					<div className="speaker-visualizer-data-calculation-container">
						<table>
							<tbody>
								<tr>
									<th>Object 1</th>
									<th>Source</th>
									<th>Object 2</th>
									<th>Angle (&deg;)</th>
								</tr>
								{
									sources.map(
										src => {
											return targets.map(
												left => {
													return targets.filter(
														cur => cur.id !== left.id
													).map(
														right => {
															return (
																<tr
																	key={j++}
																>
																	<td>{left.id}</td>
																	<td>{src.id}</td>
																	<td>{right.id}</td>
																	<td>{this._getAngle(left, src, right)}</td>
																</tr>
															);
														}
													)
												}
											)
										}
									)
								}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}
export default Visualization;