import React, {Component} from 'react';

export const SOUND_SOURCE = 'Speaker';
export const SOUND_TARGET = 'Person';

class SoundObject extends Component {
	_getObjectStyle = () => {
		const {
			highlighted,
			selected,
			type
		} = this.props;

		const SOURCE_COLOR = 'magenta';
		const TARGET_COLOR = 'cyan';
		const DEFAULT_COLOR = 'black';
		const SELECTION_COLOR = 'lightgreen';

		const objectColor = type === SOUND_SOURCE ? (
			SOURCE_COLOR
		) : (
			type === SOUND_TARGET ? (
				TARGET_COLOR
			) : (
				DEFAULT_COLOR
			)
		);

		const strokeColor = selected || highlighted ? SELECTION_COLOR : objectColor;

		return {
			'fill': objectColor,
			'stroke': strokeColor,
			'strokeWidth': 3
		};
	}

	render = () => {
		const {
			id,
			type,
			x,
			y,
			r
		} = this.props;

		const radius = r;

		return (
			<circle
					cx={x}
					cy={y}
					r={radius}
					style={this._getObjectStyle()}
			>
				<title>Object {id} ({type})</title>
			</circle>
		);
	}
}
export default SoundObject;