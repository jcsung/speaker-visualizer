import React, {Component} from 'react';

class SoundWave extends Component {
	render = () => {
		const {
            highlighted,
			x1,
            y1,
            x2,
            y2
		} = this.props;

		const stroke = highlighted ? 'green' : 'black';

		return (
			<line
                    x1={x1}
                    y1={y1}
                    x2={x2}
                    y2={y2}
					stroke={stroke}
			/>
		);
	}
}
export default SoundWave;