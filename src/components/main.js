import React, {Component} from 'react';

import Visualization from './visualization';

class Main extends Component {
	render = () => {
		return (
			<Visualization
				height={500}
				width={500}
			>
			</Visualization>
		);
	}
}
export default Main;